var gulp = require('gulp');

var rigger = require('gulp-rigger');
var promise = require('es6-promise').polyfill();

var postcss = require('gulp-postcss'),
    fs = require("fs"),
    postcssImport = require("postcss-import"),
    postcssVars = require("postcss-simple-vars"),
    postcssNested = require("postcss-nested"),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    csswring = require('csswring');

var imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

var uglify = require('gulp-uglify'),
    babel = require("gulp-babel"),
    concat = require('gulp-concat');

var lr = require('tiny-lr'),
    server = lr();

var rimraf = require('rimraf');

var browserSync = require('browser-sync'),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'static/',
        js: 'static/js/',
        style: 'static/styles/',
        img: 'static/images/',
        fonts: 'static/fonts/'
    },
    assets: {
        html: 'assets/templates/**/*.html',
        js: 'assets/js/**/*.js',
        style: 'assets/styles/**/*.css',
        img: 'assets/images/**/*.*',
        fonts: 'assets/fonts/**/*.*'
    },
    clean: './static',
    bowerDir: './bower_components' ,
};

var config = {
    server: {
        baseDir: "./static"
    },
    tunnel: true,
    host: 'localhost',
    port: 9001,
    logPrefix: "bia"
};

gulp.task('compile:html', function () {
    return gulp.src(path.assets.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('compile:css', function () {
    var processors = [
        postcssImport(),
        postcssNested,
        postcssVars,
        autoprefixer({browsers: ['last 2 version', 'ie 8-11', 'ff >= 6', 'Chrome >= 6', 'Opera >= 9', 'Safari >= 5.1.7']}),
        mqpacker,
        csswring
    ];
    return gulp.src('assets/styles/*.css')
      .pipe(sourcemaps.init())
      .pipe(postcss(processors))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(path.build.style))
      .on('error', console.log)
      .pipe(reload({stream: true}));
});

gulp.task('compile:icons', function() { 
    return gulp.src(path.bowerDir + '/font-awesome/fonts/**.*') 
        .pipe(gulp.dest(path.build.fonts)); 
});

gulp.task('compile:libs', function() {
    return gulp.src([
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/fotorama/fotorama.js',
            'bower_components/html5-polyfills/index.html',
        ])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('compile:js', function() {
    return gulp.src(path.assets.js)
        .pipe(rigger())
        .pipe(sourcemaps.init())
        .pipe(babel({ presets: ['es2015'] }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('compile:images', function() {
    return gulp.src(path.assets.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('compile:fonts', function() {
    return gulp.src(path.assets.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});

gulp.task('compile', ['compile:html', 'compile:css', 'compile:js', 'compile:images', 'compile:icons']);

gulp.task('server', function() {
    browserSync(config);
});

gulp.task('watch', function() {
    gulp.run('compile');

    gulp.watch(path.assets.html, function() {
        gulp.run('compile:html');
    });
    gulp.watch(path.assets.style, function() {
        gulp.run('compile:css');
    });
    gulp.watch(path.assets.js, function() {
        gulp.run('compile:js');
    });
    gulp.watch(path.assets.img, function() {
        gulp.run('compile:images');
    });
    gulp.watch(path.assets.fonts, function() {
        gulp.run('compile:fonts');
    });

    gulp.run('server');
});

gulp.task('just_watch', function() {
    gulp.run('compile');

    gulp.watch(path.assets.style, function() {
        gulp.run('compile:css');
    });
    gulp.watch(path.assets.js, function() {
        gulp.run('compile:js');
    });
    gulp.watch(path.assets.img, function() {
        gulp.run('compile:images');
    });
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['compile', 'server', 'watch'])
