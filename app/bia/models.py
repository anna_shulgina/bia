# -*- coding: utf-8 -*-
from django.db import models

class Service(models.Model):
    name = models.CharField(verbose_name=u'Наименование услуги', max_length=140)
    icon = models.ImageField(upload_to='services/', verbose_name=u"Превью услуги", blank=True)
    show_on_index = models.BooleanField(default=True, verbose_name=u"Отображать на главной")

    class Meta:
        verbose_name = u"Услуга"
        verbose_name_plural = u"Услуги"

    def image_img(self):
        if self.imgfile:
            return u'< img src="%s" width="100"/>' % self.imgfile.url
        else:
            return '(none)'
    image_img.short_description = 'Thumb'
    image_img.allow_tags = True

    def __unicode__(self):
        return self.name

class Direction(models.Model):
    name = models.CharField(verbose_name=u'Наименование отрасли', max_length=140)
    show_on_index = models.BooleanField(default=True, verbose_name=u"Отображать на главной")

    class Meta:
        verbose_name = u"Отрасль"
        verbose_name_plural = u"Отрасли"

    def __unicode__(self):
        return self.name

class Case(models.Model):
    title = models.CharField(verbose_name=u'Наименование', max_length=255)
    pub_date = models.DateTimeField(verbose_name=u'Дата публикации')
    description = models.TextField(verbose_name=u'Описание')
    content = models.TextField(verbose_name=u'Содержимое')
    services = models.ManyToManyField(Service, verbose_name=u'Услуги', blank=True, related_name="services")
    directions = models.ManyToManyField(Direction, verbose_name=u'Отрасли', blank=True, related_name="directions")
    preview = models.ImageField(upload_to='cases/', verbose_name=u"Превью услуги", blank=True)

    show_on_index = models.BooleanField(default=False, verbose_name=u"Отображать на главной")
    carousel_img = models.ImageField(upload_to='cases/', verbose_name=u"Превью для карусели на главной", blank=True)

    class Meta:
        verbose_name = u"Кейс"
        verbose_name_plural = u"Кейсы"

    def __unicode__(self):
        return "%s"  % self.title

class IndexPage(models.Model):
    title = models.CharField(verbose_name=u'Заголовок', max_length=140, blank=True)
    presentation = models.FileField(upload_to='files/', verbose_name=u"Файл презентации", blank=True)

    class Meta:
        verbose_name = u"Главная страница"
        verbose_name_plural = u"Главная страница"

    def __unicode__(self):
        return "%s"  % self.title

class TextItem(models.Model):
    content = models.TextField(verbose_name=u'Содержимое', max_length=500)
    place = models.ForeignKey(IndexPage, blank=True, related_name="texts")
