# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bia', '0006_auto_20160824_1511'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='indexpage',
            options={'verbose_name': '\u0413\u043b\u0430\u0432\u043d\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430', 'verbose_name_plural': '\u0413\u043b\u0430\u0432\u043d\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430'},
        ),
        migrations.AddField(
            model_name='case',
            name='carousel_img',
            field=models.ImageField(upload_to=b'cases/', verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e \u0434\u043b\u044f \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439', blank=True),
        ),
        migrations.AlterField(
            model_name='case',
            name='preview',
            field=models.ImageField(upload_to=b'cases/', verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e \u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='indexpage',
            name='presentation',
            field=models.FileField(upload_to=b'files/', verbose_name='\u0424\u0430\u0439\u043b \u043f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='textitem',
            name='place',
            field=models.ForeignKey(related_name='texts', blank=True, to='bia.IndexPage'),
        ),
    ]
