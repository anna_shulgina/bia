# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bia', '0003_auto_20160823_1731'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='case',
            options={'verbose_name': '\u041a\u0435\u0439\u0441', 'verbose_name_plural': '\u041a\u0435\u0439\u0441\u044b'},
        ),
        migrations.AlterModelOptions(
            name='direction',
            options={'verbose_name': '\u041e\u0442\u0440\u0430\u0441\u043b\u044c', 'verbose_name_plural': '\u041e\u0442\u0440\u0430\u0441\u043b\u0438'},
        ),
        migrations.AlterModelOptions(
            name='service',
            options={'verbose_name': '\u0423\u0441\u043b\u0443\u0433\u0430', 'verbose_name_plural': '\u0423\u0441\u043b\u0443\u0433\u0438'},
        ),
        migrations.AddField(
            model_name='service',
            name='icon',
            field=models.ImageField(upload_to=b'services/', verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e \u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
    ]
