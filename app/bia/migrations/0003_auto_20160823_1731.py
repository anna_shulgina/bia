# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bia', '0002_auto_20160823_1710'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='show_on_index',
            field=models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439'),
        ),
        migrations.AlterField(
            model_name='case',
            name='directions',
            field=models.ManyToManyField(to='bia.Direction', verbose_name='\u041e\u0442\u0440\u0430\u0441\u043b\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='case',
            name='services',
            field=models.ManyToManyField(to='bia.Service', verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
    ]
