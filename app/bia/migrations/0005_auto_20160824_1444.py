# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bia', '0004_auto_20160824_1214'),
    ]

    operations = [
        migrations.CreateModel(
            name='IndexPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('presentation', models.FileField(upload_to=b'/', verbose_name='\u0424\u0430\u0439\u043b \u043f\u0440\u0435\u0437\u0435\u043d\u0442\u0430\u0446\u0438\u0438', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='TextItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435')),
            ],
        ),
        migrations.AddField(
            model_name='case',
            name='preview',
            field=models.ImageField(upload_to=b'services/', verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e \u0443\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='case',
            name='pub_date',
            field=models.DateTimeField(verbose_name='\u0414\u0430\u0442\u0430 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='indexpage',
            name='text_item',
            field=models.ForeignKey(verbose_name='\u0422\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0439 \u0431\u043b\u043e\u043a', to='bia.TextItem'),
        ),
    ]
