# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('bia', '0005_auto_20160824_1444'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='indexpage',
            options={'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430', 'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430'},
        ),
        migrations.RemoveField(
            model_name='indexpage',
            name='text_item',
        ),
        migrations.AddField(
            model_name='textitem',
            name='place',
            field=models.ForeignKey(default=1, blank=True, to='bia.IndexPage'),
            preserve_default=False,
        ),
    ]
