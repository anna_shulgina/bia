# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Case, Direction, Service, TextItem, IndexPage
from django import forms
from redactor.widgets import RedactorEditor


class TextInline(admin.TabularInline):
    model = TextItem

class IndexPageAdmin(admin.ModelAdmin):
    inlines = [TextInline,]

class CaseAdminInfo(forms.ModelForm):
    class Meta:
        model = Case
        fields = '__all__'
        widgets = {
            'content': RedactorEditor(),
        }
class CaseAdmin(admin.ModelAdmin):
    list_display = ('title', )
    form = CaseAdminInfo

class DirectionAdmin(admin.ModelAdmin):
    list_display = ('name', )

class ServiceAdmin(admin.ModelAdmin):
    list_display = ('name', )

admin.site.register(Case, CaseAdmin)
admin.site.register(Direction, DirectionAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(IndexPage, IndexPageAdmin)
