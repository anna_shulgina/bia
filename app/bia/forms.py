# -*- coding: utf-8 -*-

from django import forms
from django.core.validators import RegexValidator

class MessageForm(forms.Form):
  error_css_class = 'error'

  company = forms.CharField(label=u'Компания:', max_length=100, widget=forms.TextInput(attrs={'class': 'form__row__input form__row__field', }), required=False)
  name = forms.CharField(label='Представитель:', max_length=100, widget=forms.TextInput(attrs={'class': 'form__row__input form__row__field', }), required=False)
  email = forms.EmailField(label='Email:', required=False, widget=forms.TextInput(attrs={'class': 'form__row__input form__row__field', }))

  phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Номер телефона должен соответствовать формату: '7XXXXXXXXXX'.")
  phone_number = forms.CharField(label=u'*Телефон:', help_text=u'Введите номер телефона в формате: 7XXXXXXXXXX', validators=[phone_regex], widget=forms.TextInput(attrs={'class': 'form__row__input form__row__field', }), required=True)

  message = forms.CharField(label=u'*Что бы вы хотели заказать:', max_length=250, widget=forms.Textarea(attrs={'class':'form__row__textarea form__row__field', 'rows': '6', 'cols': '40', 'resize': 'none', }), required=True)
