# -*- coding: utf-8 -*-
import json
from ast import literal_eval
from collections import MutableMapping
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.template import RequestContext
from .models import Case, Direction, Service, TextItem, IndexPage
from django.core.mail import send_mail
from .forms import MessageForm
from django.db.models import Q
from django.shortcuts import get_list_or_404, get_object_or_404

def index(request):
    directions = Direction.objects.filter(show_on_index=True)
    services = Service.objects.filter(show_on_index=True)
    content = IndexPage.objects.first()
    cases = Case.objects.filter(show_on_index=True).order_by('-pub_date')

    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            send_mail(
                subject=u"Заявка с сайта BIA",
                message=u"Компания: {company}\nПредставитель: {name}\nТелефон: {phone_number}\nEmail: {email}\nСообщение:\n{message}".format(**form.cleaned_data),
                from_email=u"from@example.com",
                recipient_list=["to@example.com"],
            )
            messages.success(request, 'Спасибо за ваше сообщение!')
            return HttpResponse(json.dumps({"success": True}), content_type="application/json")
        else:
            return HttpResponse(json.dumps(form.errors), content_type="application/json")
    else:
        form = MessageForm()

    return render(request, 'index.html', {'directions': directions, 'services': services, 'content': content, 'cases': cases, 'form': form})

def cases(request):
    form = MessageForm()
    directions = Direction.objects.filter(show_on_index=True)
    services = Service.objects.filter(show_on_index=True)

    count_start = 9
    count_more = 3

    if not request.is_ajax() and request.method == 'GET':
        if 'service' in request.GET:
            service_id = request.GET['service']
            list_of_cases = Case.objects.filter(services__id = service_id).order_by('-pub_date')[:count_start]

        if 'direction' in request.GET:
            direction_id = request.GET['direction']
            list_of_cases = Case.objects.filter(directions__id = direction_id).order_by('-pub_date')[:count_start]

        if 'service' not in request.GET and 'direction' not in request.GET:
            list_of_cases = Case.objects.order_by('-pub_date')[:count_start]


    if request.is_ajax() and request.GET:
        data = []
        count = 0
        list_of_filters = ''

        if 'count' in request.GET:
            if request.GET['count'] != 'undefined':
                count = int(request.GET['count'])
            count_more = count + count_more

        if 'filters' in request.GET:
            list_of_filters = literal_eval(literal_eval(json.dumps(request.GET['filters'], cls=DjangoJSONEncoder)))
            list_of_services = list_of_filters['services']
            list_of_directions = list_of_filters['directions']

        if count and not list_of_filters:
            cases = Case.objects.order_by('-pub_date')[count:count_more]

        if list_of_filters:
            cases = Case.objects.filter(
                Q(services__id__in=list_of_services) | Q(directions__id__in=list_of_directions)
            ).distinct().order_by('-pub_date')[count:count_more]

        for case in cases:
            case_data = {"id": case.id, "title": case.title, "preview": case.preview.url}
            data.append(case_data)

        return HttpResponse(json.dumps(data), content_type='application/json')

    return render(request, 'cases.html', {'directions': directions, 'services': services, 'cases': list_of_cases, 'form': form})

def detail(request, case_id):
    form = MessageForm()
    case = Case.objects.get(id=case_id);

    item = get_object_or_404(Case, pk=case_id)
    try:
        item_next = item.get_next_by_pub_date()
    except Case.DoesNotExist:
        item_next = None
    try:
        item_prev = item.get_previous_by_pub_date()
    except Case.DoesNotExist:
        item_prev = None

    return render(request, 'case.html', {'case': case, 'form': form, 'item_prev': item_prev, 'item_next': item_next, })
