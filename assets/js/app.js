'use strict';
let form = new Form({
  el: document.querySelector('.js-form'),
})

let filter = new Filter({
  el: document.querySelector('.js-filter'),
})

let list_of_cases = new Cases({
  el: document.querySelector('.js-cases'),
})

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

window.onload = filter.checkFilters;

// click outside the filters
document.addEventListener("click", function (e) {
  if (filter.el && filter.el.querySelectorAll('.filter__section.opened')) {
    if (filter.el.contains(e.target)) return;
    filter.closeDropdown();
  }
});
