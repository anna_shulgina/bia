'use strict';

class Cases {
  constructor(opts) {
    this.el = opts.el;
    this._initEvents();
  }

  _initEvents(elem) {
    if (this.el) {
      this.el.addEventListener('click', this._onClick.bind(this));
    }
  }

  _onClick(event) {
    let elem = event.target;

    switch (elem.dataset.action) {
      case "get_cases":
        event.preventDefault();
        filter.getFilteredCases('more');
    }
  }
}
