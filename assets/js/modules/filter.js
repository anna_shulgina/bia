'use strict';

class Filter {
  constructor(opts) {
    this.el = opts.el;

    this._initEvents();
  }

  closeDropdown(options) {
    let dropdown_list = this.el.querySelectorAll('.filter__section');
    dropdown_list.forEach(function(item) {
      if (options && item == options.element) return;
      item.classList.remove('opened');
    })
  }

  toggleDropdown(event) {
    event.preventDefault();
    let elem = event.target;
    this.closeDropdown({element: elem.closest('.filter__section')});
    elem.closest('.filter__section').classList.toggle('opened');
  }

  selectCheckboxes(event) {
    event.preventDefault();
    let elem = event.target;

    let select_param = elem.closest('.filter__section__item_group').dataset.select;
    let list = elem.closest('ul').querySelectorAll('input[type="checkbox"]')
    list.forEach(function(item) {
      item.checked = !!select_param;
    });
  }

  setCheckedParam(array, value) {
    array.forEach(function(item) {
      item.checked = value;
    });
  }

  checkFilters() {
    if (window.location.pathname == '/cases/' && window.location.search != "") {
      let service = getParameterByName('service');
      let direction = getParameterByName('direction');
      let list_of_services = document.querySelectorAll('.filter__section_services input[type="checkbox"]')
      let list_of_directions = document.querySelectorAll('.filter__section_directions input[type="checkbox"]')

      if (service) {
        filter.setCheckedParam(list_of_services, false);
        filter.setCheckedParam(list_of_directions, false);
        document.querySelector('#service-' + service).checked = true;
      }

      if (direction) {
        filter.setCheckedParam(list_of_directions, false);
        filter.setCheckedParam(list_of_services, false);
        document.querySelector('#direction-' + direction).checked = true;
      }

      if (!document.querySelectorAll('.cases_list__item').length) {
        document.querySelector('.cases_list__loading_more').setAttribute('hidden', true);
      }
    }
  }

  getFilters() {
    let filters = {
      services: [],
      directions: [],
    }

    let list_of_services = this.el.querySelectorAll('.filter__section_services input[type="checkbox"]');
    let list_of_directions = this.el.querySelectorAll('.filter__section_directions input[type="checkbox"]');

    list_of_services.forEach(function(item) {
      if (item.checked) {
        filters.services.push(item.id.slice(item.id.indexOf("-")+1, item.id.length));
      }
    });
    list_of_directions.forEach(function(item) {
      if (item.checked) {
        filters.directions.push(item.id.slice(item.id.indexOf("-")+1, item.id.length));
      }
    });
    return filters;
  }


  getFilteredCases(current_state) {
    let filters = this.getFilters();

    let parent = document.querySelector('.cases_list__container');
    let button = document.querySelector('.cases_list__loading_more');

    let count_of_items;
    if (current_state == 'more') count_of_items = parent.querySelectorAll('.cases_list__item').length;

    let params = "count=" + count_of_items + "&filters={'services':[" + filters.services + "], 'directions':[" + filters.directions + "]}";

    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/cases?' + params, true);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.send();

    xhr.onreadystatechange = function() {
      if (xhr.readyState != 4) return;
      if (xhr.status !== 200) {
				//TODO: обаботать ошибки запроса
			} else {
        let data = JSON.parse(xhr.response);
        if (!data.length) {
          if (current_state == 'start') parent.innerHTML = '<p>Кейсы по вашему запросу не найдены</p>';

          button.setAttribute('hidden', true);
        } else {
          if (current_state == 'start') {
            parent.innerHTML = '';
            button.removeAttribute('hidden');
          }
          data.forEach(function(item) {
            let element = document.createElement('div');
            element.classList.add('cases_list__item');
            element.innerHTML = `<div class="cases_list__item__image">\
                                <a href="/cases/${item.id}">\
                                  <img src="${item.preview}" alt="${item.title}" />\
                                </a>\
                              </div>\
                              <div class="cases_list__item__title">\
                                <a href="/cases/${item.id}">${item.title}</a>\
                              </div>`
            parent.appendChild(element);
          })
        }
      }
    }
  }

  _initEvents(elem) {
    let elements = document.querySelectorAll('.filter__section>a');
    if (elements.length) {
      for (let i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', this.toggleDropdown.bind(this));
      }
    }

    let selectors = document.querySelectorAll('.filter__section__item_group a');
    if (selectors.length) {
      for (let i = 0; i < selectors.length; i++) {
        selectors[i].addEventListener('click', this.selectCheckboxes.bind(this));
        selectors[i].addEventListener('click', this.getFilteredCases.bind(this, 'start'));
      }
    }

    let list_of_filters = document.querySelectorAll('input[type="checkbox"]');
    if (list_of_filters.length) {
      for (let i = 0; i < list_of_filters.length; i++) {
        list_of_filters[i].addEventListener('click', this.getFilteredCases.bind(this, 'start'));
      }
    }
  }
}
