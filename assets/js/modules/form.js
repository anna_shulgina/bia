'use strict';

//
// Find the CSRF Token cookie value
//

function parse_cookies() {
    var cookies = {};
    if (document.cookie && document.cookie !== '') {
        document.cookie.split(';').forEach(function (c) {
            var m = c.trim().match(/(\w+)=(.*)/);
            if(m !== undefined) {
                cookies[m[1]] = decodeURIComponent(m[2]);
            }
        });
    }
    return cookies;
}

class Form {
  constructor(opts) {
    this.el = opts.el;
    this.button = this.el.querySelector('button');
    this.messages = this.el.querySelector('.messages');

    this._initEvents();
  }

  showMessage(message) {
    this.messages.innerHTML = '<li>' + message + '</li>';
    this.messages.classList.remove('hidden');
  }

  hideMessage() {
    if (this.messages.classList.contains('hidden')) return;
    this.messages.classList.add('hidden');
    this.messages.innerHTML = '';
  }

  removeErrorFlags() {
    form.el.querySelectorAll('.error').forEach(function(item){
      item.classList.remove('error');
      item.removeChild(item.querySelector('.error__message'));
    });
  }

  cleanForm() {
    form.el.querySelectorAll('.form__row__field').forEach(function(item) {
      item.value = '';
    });
  }

  sendEmail() {
    let form_content = {
      company: id_company.value,
      name: id_name.value,
      email: id_email.value,
      phone_number: id_phone_number.value,
      message: id_message.value,
    }

    let params = '';
    for (let key in form_content) {
      if (form_content[key]) {
        params += '&' + key + '=' + form_content[key]
      }
    }
    params = params.slice(1, params.length);

    let cookies = parse_cookies();

    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.setRequestHeader('X-CSRFToken', cookies['csrftoken']);
    xhr.onreadystatechange = function() {
      if (xhr.readyState != 4) return;
      if (xhr.status !== 200) {
				//TODO: обаботать ошибки запроса
			} else {
        let response = JSON.parse(xhr.response)
        if (response.success) {
          form.showMessage('Спасибо за ваше сообщение!');
          form.cleanForm();
          form.removeErrorFlags();
        } else {
          form.hideMessage();
          form.removeErrorFlags();

          for (let field in response) {
            let elem = form.el.querySelector('#id_' + field).parentNode;
            elem.classList.add('error');

            let error_msg = document.createElement('div');
            error_msg.classList.add('error__message');
            error_msg.innerHTML = response[field];
            elem.appendChild(error_msg);
          }
        }

      }
    }
    xhr.send(params);
  }

  _initEvents(elem) {
    this.el.addEventListener('click', this._onClick.bind(this));
  }

  _onClick(event) {
    let elem = event.target;

    switch (elem.dataset.action) {
      case "submit":
        event.preventDefault();
        this.sendEmail();
    }
  }
}
